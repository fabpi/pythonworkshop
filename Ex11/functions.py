def square(x):
    """ This function returns a square of a number
    Parameters:
    -----------
    x : float
        Input value

    Returns:
    --------
    x2 : float
        Square of the input value
    """

    x2 = x**2

    return x2
    

print(square(10))