import numpy as np
import pylab as plt
import scipy.fftpack as sfft

x = np.arange(0,10,0.01)
a = np.sin(x*2*np.pi)
fft_a = np.abs(sfft.fft(a))

f, ax = plt.subplots(2)

ax[0].plot(a)
ax[1].plot(fft_a)
plt.show()