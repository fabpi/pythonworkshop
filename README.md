# pythonWorkshop

Files for the Python Workshop held at the FLU section on the 14th of August 2019.

Some material was taken from the DTU-Risø python course, to which we refer for a 
more structured approach and for further reading:

https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops

Fabio Pierella