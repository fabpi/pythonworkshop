import pandas as pd
import pylab as plt

### Data from:
### https://www.kaggle.com/mauryashubham/english-premier-league-players-dataset/version/1

epl = pd.read_csv('epldata_final_17-18.csv')

plt.figure()
plt.title('Age distribution')
plt.hist(epl.age, 20)

plt.figure()
plt.title('Market value distribution')
plt.hist(epl.market_value, 20)

plt.figure()
plt.title('Age vs. Market Value')
plt.plot(epl.age, epl.market_value, 'x')

plt.show()
