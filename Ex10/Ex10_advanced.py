import pandas as pd
import pylab as plt
import seaborn as sns

### Data from:
### https://www.kaggle.com/mauryashubham/english-premier-league-players-dataset/version/1

epl = pd.read_csv('epldata_final_17-18.csv')

p1 = sns.jointplot(data = epl, x = 'age', y='market_value', kind='kde')

plt.show()