import numpy as np
import pylab as plt

a = np.random.rand(100)

print(a[0:10])

plt.figure()
plt.plot(a, 'r.')
plt.show()

plt.figure()
plt.hist(a, 20)
plt.show()