import numpy as np
import pylab as plt
### Data from:
### https://datahub.io/core/global-temp#resource-monthly

a = np.loadtxt('monthly_temp_from_jan1880.csv', skiprows=1)

plt.plot(a[:,0], a[:,1])
plt.savefig('Ex9_temp.png')
plt.show()