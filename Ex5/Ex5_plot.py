import pylab as plt

a = [1,2,3]
b = [2,3,4]

plt.plot(a,b, 'r--o')
plt.title('My first plot')
plt.xlabel('Nr of apples [-]')
plt.ylabel('Nr of pears [-]')
plt.grid()
plt.savefig('Pimped.png')
plt.show()
